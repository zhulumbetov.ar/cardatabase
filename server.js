const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = 'mongodb://127.0.0.1:27017';
const databaseName = 'car_database';
const bodyParser = require('body-parser');//to insert data

// Connect to the MongoDB database
MongoClient.connect(mongoUrl, { useNewUrlParser: true }, (err, client) => {
  if (err) throw err;
  console.log('Connected to MongoDB');
  const db = client.db(databaseName);
  const carModels = db.collection('car_models');

  // Set the view engine to EJS
  app.set('view engine', 'ejs');
  app.use(express.static('public'));
  


  // Define a route for the homepage
  app.get('/', (req, res) => {
    // Retrieve all car models from the database
    carModels.find({}).toArray((err, models) => {
      if (err) throw err;
      // Render the homepage template, passing in the car models as data
      res.render('homepage', { models });
    });
  });

  // Define a route for the car detail page
  app.get('/car/:name', (req, res) => {
    // Retrieve the car model with the specified name from the database
    carModels.findOne({ name: req.params.name }, (err, model) => {
      if (err) throw err;
      if (!model) res.sendStatus(404);
      else {
        // Render the car detail template, passing in the car model as data
        res.render('car-detail', { model });
      }
    });
  });

  // Start the server
  const port = 3000;
  app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });

  app.use(bodyParser.urlencoded({ extended: false }));

  // Define a route for the add car page
  app.get('/add-car', (req, res) => {
    // Render the add car template
    res.render('add-car');
  });
  
  // Define a route to handle the form submission from the add car page
  app.post('/add-car', (req, res) => {
    const carModel = req.body['carModel'];
    const component1 = req.body['component1'];
    const component1Description = req.body['component1Description'];
    const component2 = req.body['component2'];
    const component2Description = req.body['component2Description'];
    const component3 = req.body['component3'];
    const component3Description = req.body['component3Description'];
  
  
    const newCar = {
      name: carModel,
      components: [
        { name: component1, description: component1Description },
        { name: component2, description: component2Description },
        { name: component3, description: component3Description }
      ]
    };
  
    carModels.insertOne(newCar, (err, result) => {
      if (err) throw err;
      console.log(`Added ${result.insertedCount}`);
    });
    res.redirect('/');
  });
  // Define a route for the search page
  app.get('/search', (req, res) => {
    // Render the search template
    res.render('search');
  });
  
  // Define a route to handle the form submission from the search page
  app.post('/search-components', (req, res) => {
    const componentName = req.body['componentName'];
const query = { "components.name": { $regex: componentName, $options: 'i' } };
carModels.find(query).toArray((err, compatibleModels) => {
  if (err) throw err;
  res.render('search-results', { componentName, compatibleModels });
});
    // Find all car models that have a component with the specified name
    carModels.find({ "components.name": componentName }).toArray((err, compatibleModels) => {
      if (err) throw err;
      res.render('search-results', { componentName, compatibleModels });
    });
  });

  app.get('/delete-car', (req, res) => {
    // Render the delete car template
    res.render('delete-car');
  });
  
  app.post('/delete-car', (req, res) => {
    const carModel = req.body['carModel'];
  
    // Delete the car model with the specified name from the collection
    carModels.deleteOne({ name: carModel }, (err, result) => {
      if (err) throw err;
      console.log(`Deleted ${result.deletedCount} car model`);
    });
    res.redirect('/');
  });
  
});


